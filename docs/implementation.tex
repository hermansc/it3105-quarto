\section{Implementation}

I have created three different agents, all using Python:

\begin{itemize}
\item A random player, choosing pieces and placements at random
\item A novice player, choosing pieces and placements that gives an
immidiate win - random if no such piece or placement is possible.
\item A minimax player with alpha beta pruning, evaluating states and a game
tree and deciding with different look-ahead what is the optimal move.
\end{itemize}

The algorithms for these agents are simple and easy to understand.
However while the implementation of a minimax agent is simple, the state
evaluation can be complex and hugely affect the performance of the
agent.

My code is structured in a highly object oriented fashion, where
pieces, the board, playera and the game is represented as objects
interracting with eachother. With the top-level object of one game we
can easily list all players by calling \textit{game.board.players} or list
the available pieces as \textit{game.board.pieces.available}. This makes
the code more readable, but have downsides such as demanding much memory
when creating many nodes and also more slow than many other data
structures.

A piece object has four different attributes: color, size, hole and
shape. When finding a winner we search the array of pieces for common
attributes. If four pieces in horizontal, vertical or diagonal alignment
either share four or zero attributes - the winner is the last player to
have placed a piece on the board and the game is over.
In a more lightweight fashion I could have represented the pieces as
2\textsuperscript{3} bitvalues and to find common attributes I could
have passed all pieces in an alignment through an AND-filter. In order
to find pieces not sharing attributes, I could first have passed each
piece through a XOR-filter, then all pieces through an AND-filter again.

\textbf{Performance}

A game tree consists of all possible moves and scenarios in the game.
One child in my implementation is all permutations possible for the
player in question. In all rounds (except the first and last) the
players does two things: place given piece and then choose a piece for
the opponent. without any pruning from the first possible move would have
(16!)\textsuperscript{2} nodes, which equals ~4.4*10\textsuperscript{26}
states. This is of course the correct number of states, as we do not
continue the tree after wins/losses and we with alpha beta pruning we
can reduce the brancing factor $b$ to $\sqrt{b}$, in the best case.
But even with these methods the game tree/branching factor is very large
the first moves, but then quickly decreases.

When implementing a minimax agent there are two numbers that hugely
affects the results:
\begin{itemize}
  \item Maximum tree depth of the minmax agent.
  \item When to start the minimax evaluation and tree generation.
\end{itemize}

Let's say we choose to start minimax evaluation when number of available
moves are less than or equal to 10. That is, we've already played 6
rounds. A minimax-3 agent (going 3 levels deep) constructs a tree
consisting of maximum $(10*9) * (9*8) * (8*7) = 362 000$ nodes,
while a minimax-4 agent would in worst case need to generate $(362
000) * (7*6) = 15 204 00$ nodes. That's an increase of 4200\%! more
nodes.

With an object oriented implementation my minimax-4 algorithm was too
slow and used too much memory when starting after 6 rounds, and in order
to use a reasonable amount of time I had to wait 8 rounds before
activating the minimax-4 agent. As one can see in the results (appendix
C) this made the minimax-3 agent perform better than the minimax-4 agent
in many cases. Thus the minimax-3 agent was also used in the tournament.

An obvious improvement would be to not use a novice agent the first
rounds, but rather dynamically change agents to accept more depth as the
game progresses. A scheme such as minimax-2 first two rounds, then
minimax-3 next six rounds, minimax-4 next two rounds and finally using a
minimax-5 for the last five rounds.

\textbf{Heuristics}

As evident from the tournament results (see section 3) my
utility function was too simple to beat more advanced agents, however
as will become evident the functions proves a good match and gets a
reasonable amount of 'draws'.

The utility function is dependent on the depth of the tree, as 'quick'
wins are valued more than wins in the future.

\[ u(state, depth) = \left \{
  \begin{array}{l l}
    value(state, depth)    & \quad \text{if $maximizing$ player}\\
    -value(state, depth)   & \quad \text{if $minimizing$ player}
  \end{array}
\right.\]
\[ value(state, depth) = \left \{
  \begin{array}{l l}
    10 * (maxDepth - depth + 1)    & \quad \text{if in $winning$ state}\\
    -10 * (maxDepth - depth + 1)   & \quad \text{if in $loosing$ state}\\
    0             & \quad \text{otherwise}
  \end{array}
\right.\]

where $maxDepth$ was a static variable defined depending on the agent. A
agent of minimax-4 has a maxDepth of 4. Thus if the root node in a
minimax-4 tree has a direct child which is in a winning state, this
child has the value: $10 * (4 - 1 + 1) = 40$ while a winning state in a
grand child has the value -30, and so on.

Here there are some obvious improvements that can be done. I should have
a heuristic to give a better value to nodes that neither winning nor
losing state. This could be done by counting the number of non-winning
moves in the current state.

Other alternatives to get better heuristics:

\begin{itemize}
\item Number of pieces in alignment sharing 3-common attributes.
\item How many possibilities is there for a win (maximum here is off course
three, win both in vertical, horizontal and diagonal direction).
\end{itemize}

There are also states where we are guaranteed a loose. Such a state is depicted below:

\begin{verbatim}
R     -   B     -
r     -   (b*)  -
(r*)  -   b     -
-     -   -     -

Available pieces: R*, B*, r*, b*, (R), (B), (r), (b), (b*), (r*)
\end{verbatim}

These states appears when we have two distinct 3-piece lines with at
least an opposite value of one attribute. This gives any piece to the
opponent a guaranteed loss. These states should be valued with the
lowest score possible (such as -50, for a minimax-4 agent).

\textbf{Other improvements}

There are mainly three improvements I would like to implement in order to
reduce the time complexity of my algorithm. First I would use a simplier
data structure to represent a state. In the tournament we used a
white-space seperated string consisting of 16 elements of uint8-values
and finally on the 17th index a number representing piece to be placed
on the board.

E.g. a board could look like this:

\begin{verbatim}
0xFF  0xFF  0xFF  0xFF
0xA   0xFF  0xFF  0xFF
0x4   0xFF  0xFF  0xFF
0xFF  0xFF  0xFF  0xFF
\end{verbatim}

And then at the end of the string either a piece such as: '0x5' or a
empty piece indicating that the player in question should choose a
piece: '0xFF'.

Much of the time in my algorithm is used by copying the state of one
object to another, and the manipulating it (as we do moves).

Secondly I would remove many of the nodes by not generating nodes for
symmetric boards.

Third I would implement a transposition table in order to accelerate the
generation of minimax trees. Many states calculates the exact same
children many times during e.g. 50 rounds of quarto. By using a database
of states and children, I could quickly get children (and its utility
value), instead of calculating them with depth-first.
