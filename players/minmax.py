import random
from players.novice import NovicePlayer
from game.minmax import MinMaxNode
from game.settings import *

class MinmaxPlayer(NovicePlayer):

  def __init__(self, pid, depth):
    """"
      MinmaxPlayer extending the NovicePlayer object.
    """
    super(MinmaxPlayer, self).__init__(pid)
    self.intelligence = 'minimax'
    self.depth = depth

  def getMove(self, board):
    moves = board.getPossibleMoves()
    if moves[0]["type"] != 'placement':
      # We should only get here if minimax player is the one to start a game.
      # Do a random pick.
      return super(MinmaxPlayer, self).getMove(board)

    start_depth = MINMAX_LIMITS.get(str(self), 8)
    if len(moves) <= start_depth:
      return self._doMinMax(board)
    else:
      # If we are below threshold, do a Novice move.
      return super(MinmaxPlayer, self).getMove(board)

  def _doMinMax(self, board):
    """
      Create a tree where we are the maximizing player.
    """
    tree = MinMaxNode(board,maximizingPlayer = True)
    tree.alphaBeta(self.depth,-100,100)
    return tree.best_move

  def __repr__(self):
    """
      Player name, e.g. 'minimax-3' when we have maxDepth of 3
    """
    return "%s-%d" % (self.intelligence, self.depth)
