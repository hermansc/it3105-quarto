import random
#from ete2 import Tree

class BasePlayer(object):
  def __init__(self, pid):
    self.id = pid
    self.intelligence = 'random'

  def _choose_piece(self, board):
    return [random.choice(board.getPossibleMoves())]

  def _place_piece(self, board):
    return [random.choice(board.getPossibleMoves())]

  def getMove(self, board):
    moves = board.getPossibleMoves()
    if moves[0]['type'] == 'pick':
      return self._choose_piece(board)
    elif moves[0]['type'] == 'placement':
      return self._place_piece(board)

  def __repr__(self):
    return self.intelligence
