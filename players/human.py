from players.player import BasePlayer

class HumanPlayer(BasePlayer):

  def __init__(self, pid):
    super(HumanPlayer, self).__init__(pid)
    self.intelligence = 'human'

  def _choose_piece(self, board):
    print board
    moves = board.getPossibleMoves()
    pieces = [str(move["piece"]) for move in moves]
    move = None
    while not move:
      print ",".join(pieces)
      chosen_input = raw_input('Which piece do you choose for the other player? ')
      if chosen_input in pieces:
        move = [move for move in moves if str(move["piece"]) == chosen_input][0]
      else:
        print "ERROR: Wrong input"
    return [move]

  def _place_piece(self, board):
    print board
    moves = board.getPossibleMoves()
    valid_move = None
    while not valid_move:
      chosen_row= raw_input('Which row do you want to put the piece %s on? ' % moves[0]['piece'])
      chosen_column= raw_input('Which column do you want to put the piece on? ')
      try:
        chosen_row = int(chosen_row)
        chosen_column = int(chosen_column)
        valid_columns = [move["column"] for move in moves]
        valid_rows = [move["row"] for move in moves]
        if chosen_row in valid_rows:
          if chosen_column in valid_columns:
            valid_move = [move for move in moves if move["row"] == chosen_row and move["column"] == chosen_column][0]
          else:
            print "Error: Invalid column"
        else:
          print "Error: Invalid row"
      except:
        print "Error in input. Ensure you only use numbers."
        continue
    return [valid_move]
