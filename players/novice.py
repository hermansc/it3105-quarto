import random
from players.player import BasePlayer

class NovicePlayer(BasePlayer):

  def __init__(self, pid):
    super(NovicePlayer, self).__init__(pid)
    self.intelligence = 'novice'

  def _choose_piece(self, board):
    """
      So we wan't to generate a list of possible scenarios for each of the pieces chosen.
      The state of the _board_ stayes the same after a 'pick' move, so we look ahead.
    """
    new_states = []
    moves = board.getPossibleMoves()
    for move in moves:
      s = { "madeMove": move,
            "gotState": board.makeMove(move, fake = True),
            "goodPick": False}
      new_states.append(s)

    for t in new_states:
      for move in t["gotState"].getPossibleMoves():
        tmp = t["gotState"].makeMove(move, fake = True)
        if not tmp.checkWinner():
          # Ok so the opponent can't win! It's OK to chose this one
          t["goodPick"] = True
        else:
          # If one move in a future state is a win, we don't need to check the rest.
          break

    good_picks = [state["madeMove"] for state in new_states if state["goodPick"]]
    if good_picks:
      return [random.choice(good_picks)]
    return [random.choice(moves)]

  def _place_piece(self, board):
    """
      Makes a winning move if possible, or take a random placement.
    """
    moves = board.getPossibleMoves()
    winners = [move for move in moves if board.isWinnerMove(move)]
    if winners:
      return [random.choice(winners)]
    return [random.choice(moves)]
