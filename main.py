import sys

from optparse import OptionParser

from players.novice import NovicePlayer
from players.minmax import MinmaxPlayer
from players.human import HumanPlayer
from players.player import BasePlayer
from game.game import Game
from game.board import Board

def getPlayer(t, pid):
  t = t.split("-")
  if t[0] == 'human':
    return HumanPlayer(pid)
  elif t[0] == 'novice':
    return NovicePlayer(pid)
  elif t[0] == 'minimax':
    return MinmaxPlayer(pid, int(t[1]))
  else:
    return BasePlayer(pid)

if __name__ == '__main__':
  parser = OptionParser()
  parser.add_option("-t", "--turn", help='Get move from input state')
  parser.add_option("-r", "--rounds", help='How many rounds to simulate', default=0, type="int")
  parser.add_option("-v", "--verbose", help='Verbosity level', default=1, type="int")

  (options, args) = parser.parse_args()
  if options.turn:
    player = getPlayer(args[0],1)
    b = Board([player], player)
    b.parse_string(options.turn.strip())
    moves = player.getMove(b, b.getPossibleMoves())
    if len(moves) == 1 and b.getPossibleMoves()[0]["type"] == "placement":
      # The novice player etc. returns one and one move. We want to simulate
      # both moves possible in a series.
      nextBoard = b.makeMove(moves[0])
      n = player.getMove(nextBoard, nextBoard.getPossibleMoves())
      moves += n
    s = ""
    x = y = 0
    for move in moves:
      if move["type"] == "placement":
        x = move['row']
        y = move['column']
      else:
        piece = move['piece'].uint8()
        s = "%s" % piece
      s = " ".join([s, str(x),str(y)])
    sys.stdout.write(s)
  else:
    p1 = getPlayer(args[0], 1)
    p2 = getPlayer(args[1], 2)
    c = 1
    g = Game(p1, p2)

    game_over = False
    wins = {}
    wins[str(p1)] = 0
    wins[str(p2)] = 0
    draw = 0
    while not game_over:
      # Inside the play function we find another while-loop
      status = g.play()
      if status['winner']:
        p = str(status["winner"].currentPlayer)
        if options.verbose >= 2:
          # Print the actual board in its finished state
          # Useful when debugging automatic agents.
          print status["winner"]
        wins[p] += 1
      else:
        draw += 1
      print "Winner of game %s of %s: %s. Statistics: %s: %s wins, %s: %s wins, %s draws" % (c, options.rounds, p, str(p1), wins[str(p1)], p2, wins[str(p2)], draw)
      if options.rounds > 0:
        g = Game(p1,p2)
        c += 1
        if c > options.rounds:
          game_over = True
      else:
        # When a game is over (and options.rounds is 0), we ask if we want another game
        again = raw_input("Do you want to play again? (y/N) ")
        if again.lower() in ["y", "yes"]:
          g = Game(p1, p2)
        else:
          game_over = True
