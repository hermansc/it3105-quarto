from board import Board
import random

class Game:
  def __init__(self, p1, p2):
    self.p1 = p1
    self.p2 = p2
    self.board = Board([self.p1, self.p2], random.choice([self.p1, self.p2]))

  def play(self):
    game_in_progress = True
    while game_in_progress:
      # We play!
      moves = self.board.currentPlayer.getMove(self.board)
      for move in moves:
        #if move['type'] == 'placement':
        #  print "Player %s makes placement of piece: %s at [%s,%s]" % (self.board.currentPlayer, str(move["piece"]), move["row"], move["column"])
        #else:
        #  print "Player %s picks piece: %s" % (self.board.currentPlayer, str(move["piece"]))
        self.board = self.board.makeMove(move)
        #if move["type"] == 'placement':
        #  print self.board
        if self.board.checkWinner():
          #print "Winner! %s" % self.board.currentPlayer
          #print self.board
          game_in_progress = False
          return {'winner': self.board}
        if len(self.board.getPossibleMoves()) == 0:
          game_in_progress = False
          return {'winner': None}
