from piece import Piece

class Pieces:
  def __init__(self):
    """
      Creates all possible pieces in a Quarto-game.
    """
    self._allPieces = []
    for color in ["red", "black"]:
      for size in ["big", "small"]:
        for bracketed in [True, False]:
          for starred in [True, False]:
            self._allPieces.append(Piece(color, size, bracketed, starred))

  @property
  def available(self):
    """
      Returns all pieces not placed in a board
    """
    availables = []
    for piece in self._allPieces:
      if piece.available:
        availables.append(piece)
    return availables

  def get(self, piece_identifier):
    """
      Returns a piece given a input of e.g. 'b*'
    """
    for p in self._allPieces:
      if str(p) == piece_identifier:
        return p
    return None
