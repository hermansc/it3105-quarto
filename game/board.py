from pieces import Pieces
from settings import PIECES
import cPickle

class Board:
  def __init__(self, players, currentPlayer):
    self.pieces = Pieces()
    self.slots = [[None, None, None, None] for j in range(4)]
    self.players = players
    self.currentPlayer = currentPlayer
    self.toBePlaced = None

  def getPossibleMoves(self):
    """
      Returns, given a state, all the possible moves available in this state.
      The move returned is in a dictionary format.
        * 'placement': a move containing a piece and possible x and y-values.
        * 'pick': all pieces available to pick among.
    """
    # If no possible moves are found we return an empty array
    moves = []

    # Check first if the state is where we need to place a piece on the board
    if self.toBePlaced:
      # One move for each empty slot on the board
      for slot in self.empty_slots:
        # We do a placement move, and thus need to define where to place the piece.
        move = {'type':'placement', 'piece': self.toBePlaced, 'row': slot[0], 'column': slot[1]}
        moves.append(move)
    # If not, we need to choose a piece from the availablePieces
    else:
      for piece in self.pieces.available:
        # We do a 'pick' move, and just define which piece we pick.
        move = {'type':'pick', 'piece': piece}
        moves.append(move)
    return moves

  def _getPieceFromUint8(self, number):
    """
      Translates uint8 numbers such as 0xFF to a piece, such as
      defined in my solution. See settings.py for translations.
    """
    if number == 255:
      return None
    return self.pieces.get(PIECES[number])

  def parse_string(self, stdin):
    """
      Parses input string of type:
        '255 255 255 255 255 ... 255 10'
      To a board state where the last element in the string is the
      piece toBePlaced and the first 16 elements are elements on the board.
      The values are in uint8 format.
    """
    stdin_array = stdin.split(" ")
    for i in range(len(stdin_array)):
      stdin_array[i] = self._getPieceFromUint8(int(stdin_array[i]))
    # Make it two dimensional
    self.slots = [stdin_array[i:i+4] for i in range(0, 16, 4)]
    for row in self.slots:
      for piece in row:
        if piece:
          piece.available = False
    self.toBePlaced = stdin_array[16]

  def makeMove(self, move, fake=False):
    """
      Creates a new state by copying (recursivly) the current state and then
      returns this new state with the provided move executed. The fake attributes
      has to be passed, as pieces are not copied recursivly and the piece reference need to be
      reset.
    """
    # This move is slow
    newBoard = cPickle.loads(cPickle.dumps(self, -1))
    piece = newBoard.pieces.get(str(move['piece']))
    if move['type'] == 'placement':
      newBoard._set_piece(move['row'], move['column'], piece)
      if fake: piece.available = True
      newBoard.toBePlaced = None
    elif move['type'] == 'pick':
      newBoard.toBePlaced = piece
      # When a player has picked a piece, we change current player.
      newBoard.currentPlayer = newBoard._getNextPlayer()
    else:
      raise 'Wrong move type'
    return newBoard

  def _getNextPlayer(self):
    """
      Looks at the players and returns the player that is not the current player.
      Thus the opponent.
    """
    for player in self.players:
      if player != self.currentPlayer:
        return player

  @property
  def empty_slots(self):
    """
      Returns a list of slots available for placement.
    """
    slots = []
    for row in range(len(self.slots)):
      for col in range(len(self.slots[row])):
        slot = self.slots[row][col]
        if not slot:
          slots.append((row, col))
    return slots

  def _set_piece(self, row, column, piece):
    """
      Sets piece at board and marks it as not available any more.
    """
    self.slots[row][column] = piece
    piece.available = False

  def isWinnerMove(self, move):
    """
      Create temporary state where we have executed the move
    """
    tmp = self.makeMove(move, fake = True)
    return tmp.checkWinner()

  def __str__(self):
    """"
      Prints a nice board with row and column numbers.
    """
    board_string = ""
    breadth = 37
    board_string +=  "-" * breadth + "\n"
    board_string +=  "R\C\t0\t1\t2\t3\n"
    c = 0
    s = []
    for row in self.slots:
      inner = []
      for e in [c] + row:
        inner.append(str(e))
      c += 1
      s.append(inner)
    lens = [len(max(col, key=len)) for col in zip(*s)]
    fmt = '\t'.join('{{:{}}}'.format(x) for x in lens)
    table = [fmt.format(*row) for row in s]
    board_string += '\n'.join(table)
    board_string +=  "\n" + "-" * breadth
    return board_string

  def print_pieces(self):
    print "Available pieces:"
    print ", ".join([str(p) for p in self.pieces.available])

  def _common_properties(self, pieces):
    """
      Calculates the common properties among two-dimensional array of pieces.
    """
    pieces = [p for p in pieces if p is not None]
    if not len(pieces) == 4:
      return 0
    red = 0
    small = 0
    starred = 0
    bracketed = 0
    for piece in pieces:
      if piece.color == 'red':
        red += 1
      if piece.size == 'small':
        small += 1
      if piece.bracketed:
        bracketed += 1
      if piece.starred:
        starred += 1
    d = max(red, 4-red, small, 4-small, starred, 4-starred, bracketed, 4-bracketed)
    return d

  def checkWinner(self):
    """
      Checks if we have a winner in one of the possible directions.
    """
    # Horizontally
    for row in self.slots:
      c = self._common_properties(row)
      if c == 4:
        return True

    # Vertically
    for col in [list(i) for i in zip(*self.slots)]:
      if self._common_properties(col) == 4:
        return True

    # Dioganally
    diag1 = []
    diag2 = []
    for i in range(4):
      diag1.append(self.slots[i][i])
      diag2.append(self.slots[3-i][i])
    if self._common_properties(diag1) == 4:
      return True
    if self._common_properties(diag2) == 4:
      return True
    return False
