from settings import PIECES

class Piece:
  def __init__(self, color, size, bracketed, starred):
    self.available = True
    self.color = color
    self.size = size
    self.bracketed = bracketed
    self.starred = starred

  def uint8(self):
    """
      Returns the uint8 representation of a piece.
      This is gathered from the translation dictionary in the settings.py file.
    """
    for (uint,piece) in PIECES.items():
      if str(self) == piece:
        return uint
    return 255

  def __repr__(self):
    s = ''

    # Colors
    if self.color == 'red':
      s += 'r'
    else:
      s += 'b'

    # Size
    if self.size == 'big':
      s = s.capitalize()

    # Starred
    if self.starred:
      s += '*'

    # Brackets
    if self.bracketed:
      s = '(' + s + ')'

    return s

