import random

class MinMaxNode:
  def __init__(self, board, maximizingPlayer):
    self.board = board
    self.maximizingPlayer = maximizingPlayer
    # The move(s) that got us to this state
    self.moves = []
    # The best move to take from this node
    self.best_move = []

  def alphaBeta(self, depth, alpha, beta):
    # We are in a terminal node / search bound
    if depth == 0 or self.board.checkWinner():
      return self.utility(depth)

    # Else we generate child nodes
    self.children = self.successors()

    # Check if leaf (no more moves)
    if len(self.children) == 0:
      self.bestMove = []
      return self.utility(depth)

    if self.maximizingPlayer:
      # Check alpha value
      for child in self.children:
        childAlpha = child.alphaBeta(depth-1,alpha,beta)
        if childAlpha > alpha:
          alpha = childAlpha
          self.best_move = child.moves
        if alpha >= beta:
          return alpha
      return alpha
    else:
      # Check beta value
      for child in self.children:
        childBeta = child.alphaBeta(depth-1,alpha,beta)
        if childBeta < beta:
          beta = childBeta
          self.best_move = child.moves
        if beta <= alpha:
          return beta
      return beta

  def successors(self):
    """
      Returns a list of all children to this node.
    """
    childNodes = []
    possiblePlacements = self.board.getPossibleMoves()
    for placementMove in possiblePlacements:
      newBoard = self.board.makeMove(placementMove)
      childrenPicks = newBoard.getPossibleMoves()
      if not childrenPicks:
        # If we are in a draw/final state, we end up here where only on move is possible.
        n = MinMaxNode(self.board, maximizingPlayer = not self.maximizingPlayer)
        n.moves = [placementMove]
        childNodes.append(n)
      for pickMove in childrenPicks:
        childBoard = newBoard.makeMove(pickMove)
        n = MinMaxNode(childBoard, maximizingPlayer = not self.maximizingPlayer)
        n.moves = [placementMove, pickMove]
        childNodes.append(n)
    return childNodes

  def utility(self, depth):
    """
      Our simple utility function returning high values to quick wins,
      and lower (but positive) values to wins in far future. In the same
      manner it gives a high negative value to losses in near future and
      zero point for a draw/non-winning state.

      See 'heuristics' in the report to see how this function should be
      improved.
    """
    if self.board.checkWinner():
      h_factor = 10 * (depth+1)
      if self.maximizingPlayer:
        return (-1) * h_factor
      else:
        return h_factor
    else:
      # If no-one wins, we assign the node a nutural number 0.
      return 0
