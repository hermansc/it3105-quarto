MINMAX_LIMITS = {
  'minimax-1': 16,
  'minimax-2': 13,
  'minimax-3': 11,
  'minimax-4': 9
}
PIECES = {
    0x5: 'R',
    0x1: 'B',
    0x4: 'r',
    0x0: 'b',
    0xD: 'R*',
    0x9: 'B*',
    0xC: 'r*',
    0x8: 'b*',
    0x7: '(R)',
    0x3: '(B)',
    0x6: '(r)',
    0x2: '(b)',
    0xF: '(R*)',
    0xB: '(B*)',
    0xE: '(r*)',
    0xA: '(b*)',
}
